package org.coody.framework.minicat.entity;

import java.io.IOException;

import org.coody.framework.minicat.servlet.HttpFilter;
import org.coody.framework.minicat.servlet.HttpServlet;

public final class ApplicationFilterChain {
	private int pos = 0; // 当前执行filter的offset
	private int n; // 当前filter的数量
	private HttpFilter[] filters; // filter配置类，通过getFilter()方法获取Filter
	private HttpServlet servlet;

	public void doFilter(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (pos < n) {
			HttpFilter filter = filters[pos++];
			filter.doFilter(request, response, this);
			return;
		}
		servlet.doService(request, response);
	}

}